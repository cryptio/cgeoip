// Copyright 2019 cpke
// gcc -I lib -o main main.c -I lib -lcurl -L lib/jsmn -ljsmn
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "curl/curl.h"
#include "jsmn/jsmn.h"

/**
 * GeoIPInfo represents the data returned from an ip-api.com query
 */
typedef struct GeoIPInfo {
    const char *status;
    const char *city;
    const char *zip;
    const char *country_code;
    const char *country;
    const char *region;
    const char *isp;
    const char *lon;
    const char *as;
    const char *query;
    const char *org;
    const char *lat;
    const char *timezone;
    const char *region_name;

} GeoIPInfo;

static int jsoneq(const char *json, jsmntok_t *tok, const char *s) {
    if (tok->type == JSMN_STRING && (int) strlen(s) == tok->end - tok->start &&
        strncmp(json + tok->start, s, tok->end - tok->start) == 0) {
        return 0;
    }
    return -1;
}

// https://stackoverflow.com/questions/2329571/c-libcurl-get-output-into-a-string
typedef struct string {
    char *ptr;
    size_t len;
} string;

/**
 * Initialize a string struct
 * @param s The string to initialize
 */
void init_string(string *s) {
    s->len = 0;
    s->ptr = malloc(s->len + 1);
    if (s->ptr == NULL) {
        fprintf(stderr, "malloc() failed\n");
        exit(EXIT_FAILURE);
    }
    s->ptr[0] = '\0';
}

/**
 * cURL write function callback to store the response in a string
 * @param ptr
 * @param size
 * @param nmemb
 * @param s The string to write the data to
 * @return
 */
size_t writefunc(void *ptr, size_t size, size_t nmemb, string *s) {
    size_t new_len = s->len + size * nmemb;
    s->ptr = realloc(s->ptr, new_len + 1);
    if (s->ptr == NULL) {
        fprintf(stderr, "realloc() failed\n");
        exit(EXIT_FAILURE);
    }
    memcpy(s->ptr + s->len, ptr, size * nmemb);
    s->ptr[new_len] = '\0';
    s->len = new_len;

    return size * nmemb;
}

/**
 * Returns the geo IP information for an IP address
 *
 * @param ip_address The IP address to get the information of
 * @return GeoIPInfo
 */
GeoIPInfo ip_info(const char *ip_address) {
    // Make sure the IP address actually confines to the size restraints of one to prevent buffer overflows
    assert(strlen(ip_address) <= 39);

    CURL *curl = curl_easy_init();
    GeoIPInfo info;

    if (!curl) {
        fprintf(stderr, "Could not acquire a cURL handle\n");
        return info;
    }

    string response;
    init_string(&response);

    char url[62];   // The base URL is 23 characters, and the maximum IP address length is 39 characters for IPv6
    strcpy(url, "http://ip-api.com/json/");
    strcat(url, ip_address);
    curl_easy_setopt(curl, CURLOPT_URL, url);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writefunc);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response);

    CURLcode res = curl_easy_perform(curl);
    if (res != CURLE_OK) {
        fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
        return info;
    }

    curl_easy_cleanup(curl);    // Always cleanup

    jsmn_parser json_parser;
    jsmntok_t token_buffer[128]; // We expect no more than 128 JSON tokens

    jsmn_init(&json_parser);
    int r = jsmn_parse(&json_parser, response.ptr, strlen(response.ptr), token_buffer,
                       sizeof(token_buffer) / sizeof(token_buffer[0]));
    if (r < 0) {
        printf("Failed to parse JSON: %d\n", r);
        return info;
    }

    if (r < 1 || token_buffer[0].type != JSMN_OBJECT) {
        printf("Object expected\n");
        return info;
    }

    // Loop over all keys of the root object
    for (int i = 1; i < r; i++) {
        if (jsoneq(response.ptr, &token_buffer[i], "status") == 0) {
            info.status = strndup(response.ptr + token_buffer[i + 1].start,
                                  token_buffer[i + 1].end - token_buffer[i + 1].start);
            i++;
        } else if (jsoneq(response.ptr, &token_buffer[i], "country") == 0) {
            info.country = strndup(response.ptr + token_buffer[i + 1].start,
                                   token_buffer[i + 1].end - token_buffer[i + 1].start);
            i++;
        } else if (jsoneq(response.ptr, &token_buffer[i], "countryCode") == 0) {
            info.country_code = strndup(response.ptr + token_buffer[i + 1].start,
                                        token_buffer[i + 1].end - token_buffer[i + 1].start);
            i++;
        } else if (jsoneq(response.ptr, &token_buffer[i], "region") == 0) {
            info.region = strndup(response.ptr + token_buffer[i + 1].start,
                                  token_buffer[i + 1].end - token_buffer[i + 1].start);
            i++;
        } else if (jsoneq(response.ptr, &token_buffer[i], "regionName") == 0) {
            info.region_name = strndup(response.ptr + token_buffer[i + 1].start,
                                       token_buffer[i + 1].end - token_buffer[i + 1].start);
            i++;
        } else if (jsoneq(response.ptr, &token_buffer[i], "city") == 0) {
            info.city = strndup(response.ptr + token_buffer[i + 1].start,
                                token_buffer[i + 1].end - token_buffer[i + 1].start);
            i++;
        } else if (jsoneq(response.ptr, &token_buffer[i], "zip") == 0) {
            info.zip = strndup(response.ptr + token_buffer[i + 1].start,
                               token_buffer[i + 1].end - token_buffer[i + 1].start);
            i++;
        } else if (jsoneq(response.ptr, &token_buffer[i], "lat") == 0) {
            info.lat = strndup(response.ptr + token_buffer[i + 1].start,
                               token_buffer[i + 1].end - token_buffer[i + 1].start);
            i++;
        } else if (jsoneq(response.ptr, &token_buffer[i], "lon") == 0) {
            info.lon = strndup(response.ptr + token_buffer[i + 1].start,
                               token_buffer[i + 1].end - token_buffer[i + 1].start);
            i++;
        } else if (jsoneq(response.ptr, &token_buffer[i], "timezone") == 0) {
            info.timezone = strndup(response.ptr + token_buffer[i + 1].start,
                                    token_buffer[i + 1].end - token_buffer[i + 1].start);
            i++;
        } else if (jsoneq(response.ptr, &token_buffer[i], "isp") == 0) {
            info.isp = strndup(response.ptr + token_buffer[i + 1].start,
                               token_buffer[i + 1].end - token_buffer[i + 1].start);
            i++;
        } else if (jsoneq(response.ptr, &token_buffer[i], "org") == 0) {
            info.org = strndup(response.ptr + token_buffer[i + 1].start,
                               token_buffer[i + 1].end - token_buffer[i + 1].start);
            i++;
        } else if (jsoneq(response.ptr, &token_buffer[i], "as") == 0) {
            info.as = strndup(response.ptr + token_buffer[i + 1].start,
                              token_buffer[i + 1].end - token_buffer[i + 1].start);
            i++;
        } else if (jsoneq(response.ptr, &token_buffer[i], "query") == 0) {
            info.query = strndup(response.ptr + token_buffer[i + 1].start,
                                 token_buffer[i + 1].end - token_buffer[i + 1].start);
            i++;
        } else {
            printf("Unexpected key: %.*s\n", token_buffer[i].end - token_buffer[i].start,
                   response.ptr + token_buffer[i].start);
        }

    }
    return info;
}

int main() {
    GeoIPInfo info = ip_info("8.8.8.8");
    printf("Status: %s\n", info.status);
    printf("City: %s\n", info.city);
    printf("ZIP: %s\n", info.zip);
    printf("Country code: %s", info.country_code);
    printf("Country: %s\n", info.country);
    printf("Region: %s\n", info.region);
    printf("Longitude: %s\n", info.lon);
    printf("ASN: %s\n", info.as);
    printf("Query: %s\n", info.query);
    printf("Organization: %s\n", info.org);
    printf("Latitude: %s\n", info.lat);
    printf("Timezone: %s\n", info.timezone);
    printf("Region name: %s\n", info.region_name);


    return 0;
}
